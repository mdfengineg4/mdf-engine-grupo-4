#include "TestGame.h"
#include <iostream>
#include <fstream>
#include <ostream>

using namespace std;

bool TestGame::init(DirectInput &Input){
	setRunning(true);
	input = &Input;

	quad = new Quad();

	vertex[0] = { -1.0f, -1.0f, 0.0f, D3DCOLOR_RGBA(255, 0, 0, 201) };
	vertex[1] = { -1.0f, -0.4f, 0.0f, D3DCOLOR_RGBA(0, 255, 0, 201) };
	vertex[2] = { 0.4f, -1.0f, 0.0f, D3DCOLOR_RGBA(0, 0, 255, 201) };
	vertex[3] = { 0.4f, -0.4f, 0.0f, D3DCOLOR_RGBA(255, 255, 255, 201) };

	quad->setVertex(vertex);

	quad2 = new Quad();

	vertex[0] = { -1.0f, 0.4f, 0.0f, D3DCOLOR_RGBA(255, 0, 0, 201) };
	vertex[1] = { -1.0f, 1.0f, 0.0f, D3DCOLOR_RGBA(0, 255, 0, 201) };
	vertex[2] = { 0.4f, 0.4f, 0.0f, D3DCOLOR_RGBA(0, 0, 255, 201) };
	vertex[3] = { 0.4f, 1.0f, 0.0f, D3DCOLOR_RGBA(255, 255, 255, 201) };

	quad2->setVertex(vertex);

	floor = new Quad();

	vertex[0] = { 0.4f, 0.0f, 0.0f, D3DCOLOR_RGBA(255, 0, 0, 0) };
	vertex[1] = { 0.4f, 1.0f, 0.0f, D3DCOLOR_RGBA(0, 255, 0, 0) };
	vertex[2] = { 1.0f, 0.0f, 0.0f, D3DCOLOR_RGBA(0, 0, 255, 0) };
	vertex[3] = { 1.0f, 1.0f, 0.0f, D3DCOLOR_RGBA(255, 255, 255, 0) };

	floor->setVertex(vertex);


	sprite = new Sprite(timer);

	texVertex[0] = { -0.2f, -0.2f, 0.0f, 0, 0 };
	texVertex[1] = { -0.2f, 0.2f, 0.0f, 1, 0 };
	texVertex[2] = { 0.2f, -0.2f, 0.0f, 0, 1 };
	texVertex[3] = { 0.2f, 0.2f, 0.0f, 1, 1 };

	sprite->setVertex(texVertex);

	idle = new Animation();
	play = new Animation();

	idle->setLength(1000);
	idle->addFrame(64, 16, 0, 0, 16, 16);

	play->setLength(1000);
	play->addFrame(64, 16, 16, 0, 16, 16);
	play->addFrame(64, 16, 32, 0, 16, 16);
	play->addFrame(64, 16, 48, 0, 16, 16);

	sprite->setAnimation(play);

	return true;
}

void TestGame::frame(Renderer& renderer){
	tilemap = new TileMap(renderer.loadTexture("tilemap.png", D3DCOLOR_RGBA(255, 255, 255, 255)), 128, 128, 64, 64, 10, 10);
	for (int i = 0; i < 7; i++){
		for (int j = 0; j < 10; j++){
			tilemap->setTile(0, i, j);
		}
	}

	tilemap->setTile(1, 2, 5);

	for (int i = 7; i < 8; i++){
		for (int j = 0; j < 10; j++){
			tilemap->setTile(3, i, j);
		}
	}


	for (int i = 8; i < 10; i++){
		for (int j = 0; j < 10; j++){
			tilemap->setTile(2, i, j);
		}
	}

	tilemap->draw(renderer);

	sprite->updateWorld(renderer.DxDevice);

	if (input->keyDown(Input::KEY_LEFT)){
		sprite->setAnimation(play);
		sprite->moveLeft(0.01f);
		sprite->faceLeft();

		if (sprite->collidesWith(quad))sprite->moveRight(0.01f);
	}
	else if (input->keyDown(Input::KEY_RIGHT)){
		sprite->setAnimation(play);
		sprite->moveRight(0.01f);
		sprite->faceRight();

		if (sprite->collidesWith(quad2)) sprite->moveLeft(0.01f);
	}
	else {
		if (input->keyDown(Input::KEY_UP)){
			sprite->moveUp(0.01f);
		}
		else if (input->keyDown(Input::KEY_DOWN)){
			sprite->moveDown(0.01f);
			if (sprite->collidesWith(floor))sprite->moveUp(0.01f);
		}
		sprite->setAnimation(idle);
	}

	sprite->update(timer);

	sprite->texture = renderer.loadTexture("box.png", D3DCOLOR_RGBA(255, 255, 255, 255));

	renderer.draw(sprite->vertex, sprite, TriangleStrip, 4);

	quad->updateWorld(renderer.DxDevice);

	renderer.draw(quad->vertex, TriangleStrip, 4);

	quad2->updateWorld(renderer.DxDevice);

	renderer.draw(quad2->vertex, TriangleStrip, 4);
}

bool TestGame::isRunning() const {
	return running;
}

void TestGame::setRunning(bool Running){
	running = Running;
}

void TestGame::setTimer(Timer* Timer){
	timer = Timer;
}

void TestGame::deInit(){
	delete quad;
	delete sprite;
	delete idle;
	delete play;
}
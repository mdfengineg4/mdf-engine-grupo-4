#ifndef TILE_H
#define TILE_H

#include "Entity2D.h"
#include "Misc.h"

#ifdef TILE_EXPORTS
#define TILE_API __declspec(dllexport)
#else
#define TILE_API __declspec(dllimport)
#endif

class TILE_API Tile : public Entity2D {
public:
	Tile();
	void setVertex(TilesUV uvs, float x, float y, float width, float height);

	TextureVertex vertex[4];
};
#endif //TILE_H

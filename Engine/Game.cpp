#include "Game.h"

void Game::frame(Renderer& renderer){
	renderer.draw(vertex, TriangleStrip, 4);
}

bool Game::isRunning() const {
	return running;
}

void Game::setRunning(bool Running){
	running = Running;
}

void Game::setTimer(Timer* Timer){
	timer = Timer;
}

void Game::deInit(){

}
#ifndef SPRITE_H
#define SPRITE_H

#include "Misc.h"
#include "Entity2D.h"
#include "Timer.h"
#include "Animation.h"

using namespace pg1;

#ifdef SPRITE_EXPORTS
#define SPRITE_API __declspec(dllexport)
#else
#define SPRITE_API __declspec(dllimport)
#endif

class SPRITE_API Sprite : public Entity2D {
public:
	Sprite(Timer* Timer);

	void setVertex(TextureVertex Vertex[4]);
	void setAnimation(Animation* anim);
	void update(Timer* timer);
	void setTextureCoords(float u1, float v1, float u2, float v2, float u3, float v3, float u4, float v4);
	void faceLeft();
	void faceRight();
	
	TextureVertex vertex[4];
	Timer* timer;
	Animation* animation;
	int previousFrame;
};

#endif //SPRITE_H
#include <windows.h>
#include "Engine.h"
#include "TestGame.h"
//----------------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
//----------------------------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	Engine engine(hInstance, 800, 600);

	if (!engine.init()){ //Inicializa el Engine
		return 1; //No se inicializó.
	}

	TestGame* game = new TestGame();

	engine.setGame(game);

	engine.run();

	game->deInit();

	engine.deInit();

	return 0;
}
//----------------------------------------------------------------------------------------------
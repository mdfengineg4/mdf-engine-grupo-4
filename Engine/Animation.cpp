#include "Animation.h"

#include <iostream>
#include <ostream>
#include <fstream>

Animation::Animation() {
}

void Animation::setLength(int duration){
	length = duration;
}

void Animation::addFrame(int texWidth, int texHeight, int framePosX, int framePosY, int frameWidth, int frameHeight){
	Frame newFrame;

	newFrame.u1 = (float)framePosX / (float)texWidth;
	newFrame.v1 = (float)framePosY / (float)texHeight;

	newFrame.u2 = (float)(framePosX + frameWidth) / (float)texWidth;
	newFrame.v2 = (float)framePosY / (float)texHeight;

	newFrame.u3 = (float)framePosX / (float)texWidth;
	newFrame.v3 = (float)(framePosY + frameHeight) / (float)texHeight;

	newFrame.u4 = (float)(framePosX + frameWidth) / (float)texWidth;
	newFrame.v4 = (float)(framePosY + frameHeight) / (float)texHeight;

	frames.push_back(newFrame);
}

int Animation::getCurrentFrame(){
	return currentFrame;
}

void Animation::update(Timer* timer){
	float frameLength = length / frames.size();

	currentTime += timer->timeBetweenFrames();

	while (currentTime > frameLength){
		currentTime -= frameLength;
	}

	currentFrame = static_cast<unsigned int> ((currentTime / frameLength) * frames.size());
}
#include "Tile.h"


Tile::Tile() {
	texture = NoTexture;
}

void Tile::setVertex(TilesUV uvs, float x, float y, float width, float height){
	vertex[0].u = uvs.u1;
	vertex[0].v = uvs.v1;
	vertex[0].x = x - 1;
	vertex[0].y = y - 1;
	vertex[0].z = 0;

	vertex[1].u = uvs.u2;
	vertex[1].v = uvs.v2;
	vertex[1].x = x + width - 1;
	vertex[1].y = y - 1;
	vertex[1].z = 0;

	vertex[2].u = uvs.u3;
	vertex[2].v = uvs.v3;
	vertex[2].x = x - 1;
	vertex[2].y = y + height - 1;
	vertex[2].z = 0;

	vertex[3].u = uvs.u4;
	vertex[3].v = uvs.v4;
	vertex[3].x = x + width - 1;
	vertex[3].y = y + height - 1;
	vertex[3].z = 0;
}

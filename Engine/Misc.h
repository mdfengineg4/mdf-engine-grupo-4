#ifndef MISC_H
#define MISC_H

#include <Windows.h>

struct ColorVertex {
	float x, y, z;
	DWORD color;
};

struct TextureVertex {
	float x, y, z;
	float u, v;
};

struct Frame {
	float u1, v1;
	float u2, v2;
	float u3, v3;
	float u4, v4;
};

struct TilesUV {
	float u1, v1;
	float u2, v2;
	float u3, v3;
	float u4, v4;
};

enum Primitive {
	PointList = 1,
	LineList = 2,
	LineStrip = 3,
	TriangleList = 4,
	TriangleStrip = 5,
	TriangleFan = 6
};
#endif // MISC_H
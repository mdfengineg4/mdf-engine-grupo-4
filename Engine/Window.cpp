#include "Window.h"
#include <iostream>
//----------------------------------------------------------------------------------------------
Window::Window(){

}

bool Window::create(HINSTANCE hInstance, int height, int width){
	WNDCLASS winClass;

	memset(static_cast <void *> (&winClass), 0, sizeof(WNDCLASS)); //Pone en 0 todas las variables de la ventana

	winClass.style = CS_HREDRAW | CS_VREDRAW;
	winClass.lpfnWndProc = (WNDPROC)wndProc;
	winClass.hInstance = hInstance;
	winClass.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
	winClass.lpszClassName = L"Engine";

	if (!RegisterClass(&winClass)){	//Devuelve falso si no se puede registrar la ventana.
		return false;
	}

	hWnd = CreateWindow(L"Engine",
		L"MDF Engine",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		width + 8,
		height + 30,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (!hWnd){
		return false;
	}

	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);
	return true;
}
//----------------------------------------------------------------------------------------------
void Window::setTitle(const std::string &title){
	std::wstring tempTitle = std::wstring(title.begin(), title.end());
	LPCWSTR winTitle = tempTitle.c_str();

	SetWindowText(hWnd, winTitle);
}
//----------------------------------------------------------------------------------------------
HWND Window::getHWND() const {
	return hWnd;
}
//----------------------------------------------------------------------------------------------
LRESULT CALLBACK Window::wndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam){
	switch (message){
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}
//----------------------------------------------------------------------------------------------
#include "Engine.h"
//----------------------------------------------------------------------------------------------
Engine::Engine(HINSTANCE Instance, int Height, int Width) {
	hInstance = Instance;
	width = Width;
	height = Height;
	window = new Window();
	renderer = new Renderer(Height, Width);
	input = new DirectInput();
	timer = new Timer();
}
//----------------------------------------------------------------------------------------------
Engine::~Engine(){
	delete renderer;
	renderer = NULL;

	delete window;
	window = NULL;
}
//----------------------------------------------------------------------------------------------
bool Engine::init(){
	if (!window->create(hInstance, width, height)){
		return false;
	}

	if (!renderer->init(window->getHWND())){
		return false;
	}

	if (!input->init(hInstance, window->getHWND())){
		return false;
	}

	timer->firstMeasure();

	return true;
}
//----------------------------------------------------------------------------------------------
void Engine::deInit(){
	delete timer;
	input->deinit();
	delete input;
	renderer->deInit();
	DestroyWindow(window->getHWND());
}
//----------------------------------------------------------------------------------------------
void Engine::setGame(Game* Game){
	game = Game;
	game->setTimer(timer);
}
//----------------------------------------------------------------------------------------------
void Engine::run(){
	//Loop del programa
	if (game == NULL) return;

	if (!game->init(*input)) return;

	MSG kMsg;

	while (game->isRunning()){
		//Render Frame
		timer->measure();
		renderer->beginFrame();
		input->reacquire();
		game->frame(*renderer);
		renderer->endFrame();

		if (PeekMessage(&kMsg, NULL, 0, 0, PM_REMOVE)) {
			switch (kMsg.message){
			case WM_QUIT: {
				game->setRunning(false);
				break;
			}
			}

			TranslateMessage(&kMsg);
			DispatchMessage(&kMsg);
		}
	}
}
//--------------------------------------------------------------------------------------------
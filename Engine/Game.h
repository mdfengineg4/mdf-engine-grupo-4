#ifndef GAME_H
#define GAME_H

#include "Renderer.h"
#include "DirectInput.h"
#include "Timer.h"

#ifdef GAME_EXPORTS
#define GAME_API __declspec(dllexport)
#else
#define GAME_API __declspec(dllimport)
#endif

using namespace pg1;

class __declspec(dllexport) Game {
public:
	virtual bool init(DirectInput &Input) = 0;
	virtual void frame(Renderer &renderer);
	virtual void deInit() = 0;

	virtual void setTimer(Timer* Timer) = 0;
	virtual bool isRunning() const = 0;
	virtual void setRunning(bool Running) = 0;


private:
	ColorVertex vertex[4];
	DirectInput* input;
	Timer* timer;
	bool running = true;
};

#endif // GAME_H
#ifndef TILEMAP_H
#define TILEMAP_H

#include "Tile.h"
#include "Renderer.h"
#include "Misc.h"

#ifdef TILEMAP_EXPORTS
#define TILEMAP_API __declspec(dllexport)
#else
#define TILEMAP_API __declspec(dllimport)
#endif

struct IDirect3DTexture9;

class TILEMAP_API TileMap {
	typedef IDirect3DTexture9* Texture;
public:
	TileMap(Texture Texture, int TextureWidth, int TextureHeight, int TileWidth, int TileHeight, int TilesPerRow, int TilesPerColumn);
	void setTile(int type, int xPos, int yPos);
	void draw(Renderer& renderer);

	Texture texture;
	int textureWidth;
	int textureHeight;
	int tileWidth;
	int tileHeight;
	int tilesPerRow;
	int tilesPerColumn;
	int maxTiles;

	TilesUV tilesUV[40];
	Tile* tiles[20][20];
};
#endif //TILEMAP_H

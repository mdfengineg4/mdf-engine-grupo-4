#ifndef TESTGAME_H
#define TESTGAME_H

#include "Game.h"
#include "Quad.h"
#include "Sprite.h"
#include "TileMap.h"
#include "Renderer.h"

class TestGame : public Game {
public:
	bool init(DirectInput &Input);
	void frame(Renderer &renderer);
	void deInit();

	bool isRunning() const;
	void setRunning(bool Running);
	void setTimer(Timer* Timer);
private:
	ColorVertex vertex[4];
	TextureVertex texVertex[4];
	Quad* quad;
	Quad* quad2;
	Quad* floor;
	Sprite* sprite;
	TileMap* tilemap;
	Animation* idle;
	Animation* play;
	DirectInput* input;
	Timer* timer;
	bool running = true;
};

#endif // TESTGAME_H
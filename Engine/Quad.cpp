#include "Quad.h"

#include <d3d9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

Quad::Quad() : Entity2D() {
}


void Quad::setVertex(ColorVertex Vertex[4]){
	vertex[0] = Vertex[0];
	vertex[1] = Vertex[1];
	vertex[2] = Vertex[2];
	vertex[3] = Vertex[3];

	height = (vertex[0].x - vertex[2].x) * -1;
	width = (vertex[0].y - vertex[1].y) * -1;

	y = vertex[0].x + height / 2;
	x = vertex[0].y + width / 2;
}


#include <Windows.h>
#include <string>
//-----------------------------------------------------------
#ifndef WINDOW_H
#define WINDOW_H
//-----------------------------------------------------------
#ifdef ENGINE_EXPORTS
#define ENGINE_API __declspec(dllexport)
#else
#define ENGINE_API __declspec(dllimport)
#endif
//-----------------------------------------------------------
class ENGINE_API Window {
public:
	Window();
	bool create(HINSTANCE hInstance, int height, int width);
	void setTitle(const std::string &title);

	HWND getHWND() const;	//Devuelve el handler

	static LRESULT CALLBACK wndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
private:
	HWND hWnd;
};
//-----------------------------------------------------------
#endif //WINDOW_H
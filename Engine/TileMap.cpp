#include "TileMap.h"


TileMap::TileMap(Texture Texture, int TextureWidth, int TextureHeight, int TileWidth, int TileHeight, int TilesPerRow, int TilesPerColumn) {
	texture = Texture;
	textureWidth = TextureWidth;
	textureHeight = TextureHeight;
	tileWidth = TileWidth;
	tileHeight = TileHeight;
	tilesPerRow = TilesPerRow;
	tilesPerColumn = TilesPerColumn;

	int n = 0;

	for (int i = 0; i < textureHeight; i += tileHeight){
		for (int j = 0; j < textureWidth; j += tileWidth){

			tilesUV[n].u1 = (float)j / (float)textureWidth;
			tilesUV[n].v1 = (float)i / (float)textureHeight;

			tilesUV[n].u2 = (float)(j + tileWidth) / (float)textureWidth;
			tilesUV[n].v2 = (float)i / (float)textureHeight;

			tilesUV[n].u3 = (float)j / (float)textureWidth;
			tilesUV[n].v3 = (float)(i + tileHeight) / (float)textureHeight;

			tilesUV[n].u4 = (float)(j + tileWidth) / (float)textureWidth;
			tilesUV[n].v4 = (float)(i + tileHeight) / (float)textureHeight;

			n++;
		}
	}

	for (int i = 0; i < tilesPerColumn; i++){
		for (int j = 0; j < tilesPerRow; j++){
			tiles[j][i] = new Tile();
		}
	}

	maxTiles = n;
}

void TileMap::setTile(int type, int xPos, int yPos){
	tiles[xPos][yPos]->texture = texture;

	int a = tilesPerColumn;

	tiles[xPos][yPos]->setVertex(tilesUV[type],
		(float)((float)2 / (float)tilesPerRow) * xPos,
		(float)((float)2 / (float)tilesPerColumn) * yPos,
		(float)((float)2 / (float)tilesPerRow),
		(float)((float)2 / (float)tilesPerColumn));
}

void TileMap::draw(Renderer& renderer){
	for (int i = 0; i < tilesPerColumn; i++){
		for (int j = 0; j < tilesPerRow; j++){
			if (tiles[j][i]->texture != NoTexture){
				renderer.draw(tiles[j][i]->vertex, tiles[j][i], TriangleStrip, 4);
			}
		}
	}
}

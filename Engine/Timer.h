
//---------------------------------------------------------------------------
#ifndef TIMER_H
#define TIMER_H
//---------------------------------------------------------------------------
#include <Windows.h>
//---------------------------------------------------------------------------
#ifndef TIMER_EXPORTS
#define TIMER_API __declspec(dllexport)
#else
#define TIMER_API __declspec(dllimport)
#endif
//---------------------------------------------------------------------------
namespace pg1
{
	//---------------------------------------------------------------------------
	class TIMER_API Timer
	{
		// constructor
	public:
		Timer();


		// measure
	public:
		void firstMeasure();
		void measure();

		float timeBetweenFrames() const;
		unsigned int fps() const;

	private:
		double m_dTimeBetweenFrames;
		double m_dMeasureFpsSample;
		unsigned int m_uiFPS;
		unsigned int m_uiFrameCounter;

		LARGE_INTEGER m_kPerfCount1;
		LARGE_INTEGER m_kPerfCount2;
		LARGE_INTEGER m_kFrequency;
	};
	//---------------------------------------------------------------------------
#include "Timer.inl"
	//---------------------------------------------------------------------------
} // end namespace
//---------------------------------------------------------------------------
#endif // TIMER_H
//---------------------------------------------------------------------------

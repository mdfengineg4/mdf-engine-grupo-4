#ifndef ENTITY2D_H
#define ENTITY2D_H

#define NoTexture NULL

#ifdef ENTITY2D_EXPORTS
#define ENTITY2D_API __declspec(dllexport)
#else
#define ENTITY2D_API __declspec(dllimport)
#endif

struct IDirect3DDevice9;
struct D3DXMATRIX;
struct IDirect3DTexture9;

class ENTITY2D_API Entity2D {
public:
	Entity2D();

	void setRotation(float Rotation);
	void setScale(float X, float Y);
	void setXY(float X, float Y);
	void moveLeft(float val);
	void moveRight(float val);
	void moveUp(float val);
	void moveDown(float val);
	bool collidesWith(Entity2D* other);

	float x = 0;
	float y = 0;
	float width = 0;
	float height = 0;
	float scaleX = 1;
	float scaleY = 1;

	void updateWorld(IDirect3DDevice9 *DxDevice);

	float getRotation();

	typedef IDirect3DTexture9* Texture;
	Texture texture;
private:
	typedef D3DXMATRIX* Matrix;

	Matrix m_World;
	Matrix m_Translation;
	Matrix m_Rotation;
	Matrix m_Scale;

	float rotation = -1.5718f;
};

#endif
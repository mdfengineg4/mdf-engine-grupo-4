#ifndef QUAD_H
#define QUAD_H

#include "Misc.h"
#include "Entity2D.h"

#ifdef QUAD_EXPORTS
#define QUAD_API __declspec(dllexport)
#else
#define QUAD_API __declspec(dllimport)
#endif

class QUAD_API Quad : public Entity2D {
public:
	Quad();

	void setVertex(ColorVertex Vertex[4]);
	ColorVertex vertex[4];
};

#endif //QUAD_H
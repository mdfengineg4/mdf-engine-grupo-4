#ifndef RENDERER_H
#define RENDERER_H
//----------------------------------------------------------------------------------------------
#include <Windows.h>
#include <string>
#include <array>
#include <vector>
#include "VertexBuffer.h"
#include "Misc.h"
#include "Entity2D.h"

#include <d3d9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
//----------------------------------------------------------------------------------------------
#ifdef RENDERER_EXPORTS
#define RENDERER_API __declspec(dllexport)
#else
#define RENDERER_API __declspec(dllimport)
#endif
//----------------------------------------------------------------------------------------------
class RENDERER_API Renderer {
public:
	Renderer(unsigned int Height, unsigned int Width);
	~Renderer();

	bool init(HWND hWnd);
	void draw(ColorVertex* vertices, Primitive primitive, size_t nVertex);	//ColorVertex
	void draw(TextureVertex* vertices, Entity2D* sprite, Primitive primitive, size_t nVertex);//TextureVertex
	void deInit();

	void beginFrame();
	void endFrame();

	const Entity2D::Texture loadTexture(const std::string &filename, int chromaKey);
	void setCurrentTexture(const Entity2D::Texture &texture);

	IDirect3DDevice9* DxDevice;
	//----------------------------------------------------------------------------------------------
private:
	IDirect3D9* DxObject;
	VertexBuffer* colorVertexBuffer;
	VertexBuffer* textureVertexBuffer;

	float translation = 0.0f;
	float rotation = 0.0f;

	unsigned int width;
	unsigned int height;

	void setDXParameters(D3DPRESENT_PARAMETERS &d3DPresentParameters);
	void setProjectionView();

	std::vector<Entity2D::Texture> arrayTextures;
};
//----------------------------------------------------------------------------------------------
#endif //RENDERER_H
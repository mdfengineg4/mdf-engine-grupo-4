#include "Entity2D.h"

#include <d3d9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

Entity2D::Entity2D() : m_World(new D3DXMATRIX()), m_Translation(new D3DXMATRIX()), m_Rotation(new D3DXMATRIX()), m_Scale(new D3DXMATRIX()) {
	D3DXMatrixRotationZ(m_Rotation, rotation);
	D3DXMatrixTranslation(m_Translation, x, y, 0);
	D3DXMatrixScaling(m_Scale, 1, 1, 1);
}

void Entity2D::setRotation(float Rotation){
	rotation = Rotation;
	D3DXMatrixRotationZ(m_Rotation, rotation);
}

void Entity2D::setScale(float X, float Y){
	scaleX = X;
	scaleY = Y;

	D3DXMatrixScaling(m_Scale, X, Y, 1);
}

void Entity2D::setXY(float X, float Y){
	x = X;
	y = Y;

	D3DXMatrixTranslation(m_Translation, y, x, 0);
}

void Entity2D::moveLeft(float val){
	x -= val;

	D3DXMatrixTranslation(m_Translation, y, x, 0);
}

void Entity2D::moveRight(float val){
	x += val;

	D3DXMatrixTranslation(m_Translation, y, x, 0);
}

void Entity2D::moveUp(float val){
	y -= val;

	D3DXMatrixTranslation(m_Translation, y, x, 0);
}

void Entity2D::moveDown(float val){
	y += val;

	D3DXMatrixTranslation(m_Translation, y, x, 0);
}

void Entity2D::updateWorld(IDirect3DDevice9 *DxDevice){
	D3DXMATRIX* temp = new D3DXMATRIX();

	D3DXMatrixMultiply(temp, m_Translation, m_Rotation);
	D3DXMatrixMultiply(m_World, temp, m_Scale);
	DxDevice->SetTransform(D3DTS_WORLD, m_World);
}

float Entity2D::getRotation(){
	return rotation;
}

bool Entity2D::collidesWith(Entity2D* other){
	if (other->x > 0.2f){
		int a = 1;
	}

	float ent1X = x - width / 2;	
	float ent1Y = y - height / 2;	
	float ent2X = other->x - other->width / 2;
	float ent2Y = other->y - other->height / 2;

	bool xCollides = false;
	bool yCollides = false;

	if (ent1X > ent2X && ent1X < ent2X + other->width){	
		xCollides = true;
	}

	if (ent1X + width > ent2X && ent1X + width < ent2X + other->width){ 
		xCollides = true;
	}

	if (ent1Y > ent2Y && ent1Y < ent2Y + other->height){ 
		yCollides = true;
	}

	if (ent1Y + height > ent2Y && ent1Y + height < ent2Y + other->height){ 
		yCollides = true;
	}

	if (xCollides && yCollides) return true;

	return false;
}
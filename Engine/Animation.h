#ifndef ANIMATION_H
#define ANIMATION_H

#include "Misc.h"
#include "Timer.h"
#include <vector>

using namespace pg1;

#ifndef ANIMATION_EXPORTS
#define ANIMATION_API __declspec(dllexport)
#else
#define ANIMATION_API __declspec(dllimport)
#endif

class ANIMATION_API Animation {
public:
	Animation();
	void setLength(int duration);
	void addFrame(int texWidth, int texHeight, int framePosX, int framePosY, int frameWidth, int frameHeight);
	void update(Timer* timer);
	int getCurrentFrame();

	std::vector<Frame> frames;

	float currentTime;
	int currentFrame;
	int length;
};

#endif //ANIMATION_H

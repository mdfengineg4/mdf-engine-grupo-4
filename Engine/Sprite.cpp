#include "Sprite.h"

#include <d3d9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

Sprite::Sprite(Timer* Timer) : Entity2D() {
	texture = NoTexture;
	animation = NULL;
	timer = Timer;
}

void Sprite::setVertex(TextureVertex Vertex[4]){
	vertex[0] = Vertex[0];
	vertex[1] = Vertex[1];
	vertex[2] = Vertex[2];
	vertex[3] = Vertex[3];

	height = (vertex[0].x - vertex[2].x) * -1;
	width = (vertex[0].y - vertex[1].y) * -1;

	x = vertex[0].x + width / 2;
	y = vertex[0].y + height / 2;
}

void Sprite::setAnimation(Animation* anim){
	animation = anim;
	previousFrame = 9999;
}

void Sprite::setTextureCoords(float u1, float v1, float u2, float v2, float u3, float v3, float u4, float v4){
	vertex[0].u = u1;
	vertex[0].v = v1;

	vertex[1].u = u2;
	vertex[1].v = v2;

	vertex[2].u = u3;
	vertex[2].v = v3;

	vertex[3].u = u4;
	vertex[3].v = v4;
}

void Sprite::faceLeft(){
	vertex[0].y = abs(vertex[0].y);
	vertex[1].y = -abs(vertex[1].y);
	vertex[2].y = abs(vertex[2].y);
	vertex[3].y = -abs(vertex[3].y);
}

void Sprite::faceRight(){
	vertex[0].y = -abs(vertex[0].y);
	vertex[1].y = abs(vertex[1].y);
	vertex[2].y = -abs(vertex[2].y);
	vertex[3].y = abs(vertex[3].y);
}

void Sprite::update(Timer* timer){
	if (animation == NULL) return;

	animation->update(timer);

	int currentFrame = animation->getCurrentFrame();

	if (currentFrame != previousFrame){
		Frame curFrame = animation->frames[currentFrame];
		setTextureCoords(curFrame.u1, curFrame.v1,
			curFrame.u2, curFrame.v2,
			curFrame.u3, curFrame.v3,
			curFrame.u4, curFrame.v4);
		previousFrame = currentFrame;
	}
}
#include "Renderer.h"

Renderer::Renderer(unsigned int Height, unsigned int Width) {
	DxObject = NULL;
	DxDevice = NULL;

	height = Height;
	width = Width;
}

Renderer::~Renderer() {
}

void Renderer::deInit(){
	DxDevice->Release();
	DxObject->Release();
	delete colorVertexBuffer;
	delete textureVertexBuffer;

	for (int i = 0; i < arrayTextures.size(); i++){
		arrayTextures[i]->Release();
	}
}

bool Renderer::init(HWND hWnd){
	DxObject = Direct3DCreate9(D3D_SDK_VERSION);

	D3DPRESENT_PARAMETERS d3DPresentParameters;
	ZeroMemory(&d3DPresentParameters, sizeof(d3DPresentParameters));
	setDXParameters(d3DPresentParameters);
	d3DPresentParameters.hDeviceWindow = hWnd;

	HRESULT hr = DxObject->CreateDevice(	//HRESULT es como un sistema de errores de Windows
		D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING,
		&d3DPresentParameters,
		&DxDevice);

	if (hr != D3D_OK){	//Si el HRESULT devuelve cualquier cosa que no sea OK, devuelve false
		return false;
	}

	DxDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	DxDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	DxDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	DxDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	DxDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	DxDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	DWORD ColorVertexType = (D3DFVF_XYZ | D3DFVF_DIFFUSE);
	DWORD TextureVertexType = (D3DFVF_XYZ | D3DFVF_TEX1);

	colorVertexBuffer = new VertexBuffer(DxDevice, sizeof(ColorVertex), ColorVertexType);
	textureVertexBuffer = new VertexBuffer(DxDevice, sizeof(TextureVertex), TextureVertexType);

	setProjectionView();

	return true;
}

void Renderer::setProjectionView(){
	D3DXMATRIX orthographixMatrix;

	D3DXMatrixOrthoLH(&orthographixMatrix, 2.0f, (height / width) * 2, 0.0f, 20.0f);

	DxDevice->SetTransform(D3DTS_PROJECTION, &orthographixMatrix);
}

void Renderer::beginFrame(){
	DxDevice->BeginScene();

	DxDevice->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
}

void Renderer::endFrame(){
	colorVertexBuffer->flush();
	textureVertexBuffer->flush();

	DxDevice->EndScene();
	DxDevice->Present(NULL, NULL, NULL, NULL);
}

void Renderer::draw(ColorVertex* vertices, Primitive primitive, size_t nVertex){
	setCurrentTexture(NoTexture);

	colorVertexBuffer->bind();
	colorVertexBuffer->draw(vertices, (D3DPRIMITIVETYPE)primitive, nVertex);
}

void Renderer::draw(TextureVertex* vertices, Entity2D* sprite, Primitive primitive, size_t nVertex){
	setCurrentTexture(sprite->texture);

	textureVertexBuffer->bind();
	textureVertexBuffer->draw(vertices, (D3DPRIMITIVETYPE)primitive, nVertex);
}

void Renderer::setDXParameters(D3DPRESENT_PARAMETERS &d3DPresentParameters){
	d3DPresentParameters.MultiSampleType = D3DMULTISAMPLE_NONE;
	d3DPresentParameters.MultiSampleQuality = 0;
	d3DPresentParameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3DPresentParameters.Windowed = true;
	d3DPresentParameters.EnableAutoDepthStencil = true;
	d3DPresentParameters.AutoDepthStencilFormat = D3DFMT_D16;
	d3DPresentParameters.Flags = NULL;
	d3DPresentParameters.FullScreen_RefreshRateInHz = 0;
	d3DPresentParameters.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
}

const Entity2D::Texture Renderer::loadTexture(const std::string &filename, int chromaKey){
	Entity2D::Texture texture = NoTexture;

	std::wstring stemp = std::wstring(filename.begin(), filename.end());
	LPCWSTR sw = stemp.c_str();

	HRESULT hr = D3DXCreateTextureFromFileEx(DxDevice,
		sw,
		0, 0, 0, 0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_NONE, D3DX_FILTER_NONE,
		chromaKey,
		NULL, NULL,
		(LPDIRECT3DTEXTURE9*)&texture);

	if (hr != D3D_OK){
		return NoTexture;
	}
	else {
		arrayTextures.push_back(texture); //(Es un vector de texturas) (Es para que se destruyan las texturas en el delete, con un iterador que haga textures.end)
		return texture;
	}
}

void Renderer::setCurrentTexture(const Entity2D::Texture &texture){
	DxDevice->SetTexture(0, texture);
}
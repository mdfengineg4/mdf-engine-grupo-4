#include <Windows.h>

#include "Window.h"
#include "Renderer.h"
#include "Game.h"
#include "DirectInput.h"
#include "Input.h"
#include "Timer.h"
//----------------------------------------------------------------------------------------------
#ifndef ENGINE_H
#define ENGINE_H
//----------------------------------------------------------------------------------------------
#ifdef ENGINE_EXPORTS
#define ENGINE_API __declspec(dllexport)
#else
#define ENGINE_API __declspec(dllimport)
#endif
//----------------------------------------------------------------------------------------------
using namespace pg1;
//----------------------------------------------------------------------------------------------
class ENGINE_API Engine {
public:
	Engine(HINSTANCE Instance, int Height, int Width);
	~Engine();

	bool init();
	void deInit();
	void run();
	void setGame(Game* Game);
	//----------------------------------------------------------------------------------------------
private:
	HINSTANCE hInstance;
	unsigned int width;
	unsigned int height;

	Window* window;
	Renderer* renderer;
	Game* game;
	DirectInput* input;
	Timer* timer;
};
//----------------------------------------------------------------------------------------------
#endif //ENGINE_H